# Project 2 - Webcam Mirror #

![](/screenshot.jpg)

In this project I wanted to play with the idea of being able to see your "words" and emotions. As someone talks more 
or opens their mouth more, they are able to see more lines and noise created by their actions. The user is also able to change
the color of the lines or "words" by changing the position of their eyebrows. This change was supposed to relate to mood, 
however, the colors that ended up coming out of it were not necessarily related to that. I utilized some code from the internet
to help create the sphere and moving lines. I made my own changes to the code to allow the color of the lines to be dictated by 
where someone's eyebrows were rather than where the line was on the sphere. I also changed how the sphere's location was 
determined and fixed it to the center point of someone's head. 