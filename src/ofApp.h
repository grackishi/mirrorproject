#pragma once

#include "ofMain.h"
#include "ofxFaceTracker2.h"
#include "ofxGui.h"

class ofApp : public ofBaseApp {

public:
	void setup();
	void update();
	void draw();

	ofVideoGrabber grabber;
	ofxFaceTracker2 tracker;
	//ofxFaceTracker2Landmarks landmarks;
	// Set Up Frame

	// Set Up GUI
	ofxPanel gui;
	ofxFloatSlider floatySlider;
	ofxFloatSlider red;
	ofxFloatSlider blue;
	ofxFloatSlider green;
	ofxFloatSlider opacity;
	ofxToggle random;
	 

	int locX;
	int locY;
	int locZ;
	 
	float xInc;
	float yInc;

	float mDist;
	float b2Dist;
	float bDist;
	
};
