#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup() {
	// Setup grabber
	grabber.setup(1280, 720);

	//Because some code is borrowed from example code
	ofSetDataPathRoot("data/model/");

	// Setup tracker
	tracker.setup();

	// Variables
	locX = 0;
	locY = 0;
	locZ = 0;
	xInc = 10;


}

//--------------------------------------------------------------
void ofApp::update() {
	grabber.update();

	// Update tracker when there are new frames
	if (grabber.isFrameNew()) {
		if (locX > 100) {
			xInc = -1;
		}
		if (locX < -100) {
			xInc = 1;
		}
		locX += xInc;
		tracker.update(grabber);
	}

	// Update for each frame for faces
	for (auto face : tracker.getInstances()) {

		// Get so you can access landmarks
		ofxFaceTracker2Landmarks landmarks = face.getLandmarks();

		// Eye and brow points
		glm::vec2 lEyeT = landmarks.getImagePoint(39); 
		glm::vec2 rEyeT = landmarks.getImagePoint(42);
		glm::vec2 lBrow = landmarks.getImagePoint(21); 
		glm::vec2 rBrow = landmarks.getImagePoint(22);

		//Mouth points
		glm::vec2 topMouth = landmarks.getImagePoint(62);
		glm::vec2 botMouth = landmarks.getImagePoint(66);

		// Calculate distances beetween the mouth points and brow points
		mDist = ofDist(topMouth.x, topMouth.y, botMouth.x, botMouth.y);
		b2Dist = ofDist(rBrow.x, rBrow.y, rEyeT.x, rEyeT.y);
		bDist = ofDist(lBrow.x, lBrow.y, lEyeT.x, lEyeT.y);

	//	std::cout << b2Dist << endl;
	}
}

//--------------------------------------------------------------
void ofApp::draw() {
	// Draw camera image
	grabber.draw(0, 0);

	ofPushStyle();

	// Iterate over all faces
	for (auto face : tracker.getInstances()) {
		// Apply the pose matrix
		ofPushView();
		face.loadPoseMatrix();

		// Based on code from jun kiyoshi - https://junkiyoshi.com
		auto base_noise_seed_x = ofRandom(1000);
		auto base_noise_seed_y = ofRandom(1000);
		glm::vec3 base;

		// Changed raidus, noise step functions
		auto radius = 130;
		float noise_step = mDist/1000; //.003 //Want this as an interactive object
		float len = 5;
		ofColor color;
		for (auto deg = 0; deg < 360; deg += 5) {

			auto noise_seed_x = ofRandom(1000);
			auto noise_seed_y = ofRandom(1000);
			auto noise_seed_z = ofRandom(1000);

			// Changed color to be affected by eyebrows
			color.setHsb(b2Dist*4, 255, 255);

			glm::vec3 pre_location;
			for (int i = 0; i < len; i++) {

				auto angle_x = ofMap(ofNoise(noise_seed_x, (ofGetFrameNum() + i) * noise_step), 0, 1, -PI * 2, PI * 2);
				auto angle_y = ofMap(ofNoise(noise_seed_y, (ofGetFrameNum() + i) * noise_step), 0, 1, -PI * 2, PI * 2);
				auto angle_z = ofMap(ofNoise(noise_seed_z, (ofGetFrameNum() + i) * noise_step), 0, 1, -PI * 2, PI * 2);

				auto location = glm::vec3(radius * cos(deg * DEG_TO_RAD), radius * sin(deg * DEG_TO_RAD), 0);
				auto rotation_x = glm::rotate(glm::mat4(), angle_x, glm::vec3(1, 0, 0));
				auto rotation_y = glm::rotate(glm::mat4(), angle_y, glm::vec3(0, 1, 0));
				auto rotation_z = glm::rotate(glm::mat4(), angle_z, glm::vec3(0, 0, 1));
				location = glm::vec4(location, 0) * rotation_z * rotation_y * rotation_x;

				// Took out the sphere movement around the page

			/*	base = glm::vec3(ofMap(ofNoise(base_noise_seed_x, (ofGetFrameNum() + i) * 0.005), 0, 1, -250, 250),
					ofMap(ofNoise(base_noise_seed_y, (ofGetFrameNum() + i) * 0.005), 0, 1, -250, 250), 0);

				location += base; */


				if (i > 0) {

					ofSetColor(color);
					ofDrawLine(location, pre_location);
				}
				pre_location = location;
			}

		}
		ofPopView();
	}
	ofPopStyle();

	ofDrawBitmapStringHighlight("Tracker fps: " + ofToString(tracker.getThreadFps()), 10, 20);

	gui.draw();
}
